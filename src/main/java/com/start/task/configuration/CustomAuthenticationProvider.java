package com.start.task.configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.start.task.models.User;
import com.start.task.repositories.UserRepository;

@Configuration
@EnableWebSecurity
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	UserRepository userReposiroty;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String name = authentication.getName();
		String password = authentication.getCredentials().toString();
		User user = userReposiroty.findByUserNameAndPassword(name, password);
		if (user == null || "".equals(user.getPassword()))
			throw new BadCredentialsException("Error username & password, Please try again !");

		final List<GrantedAuthority> grantedAuths = new ArrayList<>();
		grantedAuths.addAll(fetchUserRolesfromThe(user));
		final Authentication auth = new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
		return auth;
	}

	private Collection<? extends GrantedAuthority> fetchUserRolesfromThe(User user) {
		Set<String> roles = user.getRoles();
		Set<SimpleGrantedAuthority> authorities = new HashSet<SimpleGrantedAuthority>();
		for (String role : roles)
			authorities.add(new SimpleGrantedAuthority(role));

		return authorities;
	}

	@Override
	public boolean supports(Class<?> authentication) {

		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
