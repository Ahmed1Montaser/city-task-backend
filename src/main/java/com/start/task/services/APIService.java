package com.start.task.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.start.task.dtos.CityDTO;
import com.start.task.models.City;

@Service
public interface APIService {

	List<City> fetchByName(String word);

	List<City> fetchPageNumber(Integer pageNumber);

	City editCity(CityDTO city);

	CityDTO findById(String id);
}
