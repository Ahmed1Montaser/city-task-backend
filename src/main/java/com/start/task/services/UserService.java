package com.start.task.services;

import org.springframework.stereotype.Service;

@Service
public interface UserService {
	void createDummyUser();
}
