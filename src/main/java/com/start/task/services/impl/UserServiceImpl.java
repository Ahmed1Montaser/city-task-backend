package com.start.task.services.impl;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.start.task.models.User;
import com.start.task.repositories.UserRepository;
import com.start.task.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public void createDummyUser() {
		User u = new User();
		u.setUserName("admin");
		u.setPassword("admin");
		u.setRoles(Stream.of("ROLE_ALLOW_EDIT").collect(Collectors.toSet()));
		userRepository.save(u);
		u = new User();
		u.setUserName("user");
		u.setPassword("user");
		userRepository.save(u);
	}

}
