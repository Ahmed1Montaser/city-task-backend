package com.start.task.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.start.task.dtos.CityDTO;
import com.start.task.models.City;
import com.start.task.repositories.CityRepository;
import com.start.task.services.APIService;

@Service
public class APIServiceImpl implements APIService {

	@Autowired
	CityRepository cityRepository;

	@Override
	public List<City> fetchByName(String word) {
		return cityRepository.fechbyName(word);
	}

	@Override
	public List<City> fetchPageNumber(Integer pageNumber) {
		return cityRepository.fetchPageNumber((pageNumber * 10));
	}

	@Override
	public City editCity(CityDTO cityDTO) {
		City city = cityRepository.findById(cityDTO.getId()).get();
		city.setName(cityDTO.getName());
		city.setPhoto(cityDTO.getPhoto());
		cityRepository.save(city);
		return cityRepository.findById(cityDTO.getId()).get();
	}

	@Override
	public CityDTO findById(String id) {
		return CityDTO.toDTO(cityRepository.findById(id).get());
	}

}
