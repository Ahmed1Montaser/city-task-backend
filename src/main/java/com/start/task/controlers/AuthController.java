package com.start.task.controlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.start.task.dtos.UserRequestDTO;
import com.start.task.dtos.UserResponseDTO;
import com.start.task.utils.JwtUtils;

@RestController
@RequestMapping("/api")
public class AuthController {

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	private AuthenticationManager authenticationManager;

	@PostMapping("/authenticate")
	public ResponseEntity<?> authinticatUser(@RequestBody UserRequestDTO req) {
		String token = jwtUtils.generateToken(req.getUserName());
		UserResponseDTO dto = new UserResponseDTO();
		dto.setJwttocken(token);
		authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(req.getUserName(), req.getPassword()));
		return ResponseEntity.ok(dto);
	}
	
	@PostMapping("/isvalid-token/{jwt}")
	public boolean isValidToken(@PathVariable("jwt") String jwt) {
		return !jwtUtils.isTokenExpired(jwt);
	}

}
