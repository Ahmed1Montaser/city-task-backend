package com.start.task.controlers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.start.task.dtos.CityDTO;
import com.start.task.models.City;
import com.start.task.services.APIService;

@RestController
@RequestMapping("/api")
public class CityController {

	@Autowired
	APIService service;

	@GetMapping("/city-name/{city}")
	public List<City> fetchAll(@PathVariable("city") String city) {
		return service.fetchByName(city);
	}

	@GetMapping("/city-id/{id}")
	public CityDTO fetchById(@PathVariable("id") String id) {
		return service.findById(id);
	}

	@GetMapping(value = "/page/{page}")
	public List<City> paginate(@PathVariable("page") Integer page) {
		return service.fetchPageNumber(page);
	}

	@PutMapping(value = "/edit/city")
	@PreAuthorize("hasAuthority('ROLE_ALLOW_EDIT')")
	public CityDTO editCity(@RequestBody CityDTO cityDTO) {
		return CityDTO.toDTO(service.editCity(cityDTO));
	}
}
