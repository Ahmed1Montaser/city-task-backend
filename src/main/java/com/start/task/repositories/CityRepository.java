package com.start.task.repositories;

import java.util.List;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.start.task.models.City;

@Repository
public interface CityRepository extends Neo4jRepository<City, String> {

	@Query("MATCH (n:City) WHERE  toLower(n.name) CONTAINS toLower({word}) RETURN n")
	List<City> fechbyName(@Param("word") String word);

	@Query("MATCH (n:City) RETURN n skip {page} limit 10")
	List<City> fetchPageNumber(@Param("page") int page);

	@Query("LOAD CSV WITH HEADERS FROM \"file:///cities.csv\" AS row create(node:City {id:row.id,name:row.name,photo:row.photo})")
	void createCities();

	@Query("MATCH (n) DETACH DELETE n")
	void deleteAllNodes();
}
