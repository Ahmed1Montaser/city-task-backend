package com.start.task.repositories;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import com.start.task.models.User;

@Repository
public interface UserRepository extends Neo4jRepository<User, Long> {
	User findByUserNameAndPassword(String userName, String password);

	User findByUserName(String username);
}
