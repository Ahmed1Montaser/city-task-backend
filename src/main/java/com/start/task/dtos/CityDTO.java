package com.start.task.dtos;

import com.start.task.models.City;

public class CityDTO {

	private String id;
	private String name;
	private String photo;

	public CityDTO() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public static CityDTO toDTO(City c) {
		CityDTO dto = new CityDTO();
		dto.setId(c.getId());
		dto.setName(c.getName());
		dto.setPhoto(c.getPhoto());
		return dto;
	}

}
