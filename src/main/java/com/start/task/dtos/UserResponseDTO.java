package com.start.task.dtos;

public class UserResponseDTO {
	private String jwttocken;

	public UserResponseDTO() {

	}

	public String getJwttocken() {
		return jwttocken;
	}

	public void setJwttocken(String jwttocken) {
		this.jwttocken = jwttocken;
	}

}
