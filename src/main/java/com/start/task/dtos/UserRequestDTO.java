package com.start.task.dtos;

import java.io.Serializable;

public class UserRequestDTO implements Serializable {

	private static final long serialVersionUID = -3403462213943295914L;
	private String password;
	private String userName;

	public UserRequestDTO() {

	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
