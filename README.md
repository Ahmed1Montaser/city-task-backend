# README #

This README would normally document whatever steps are necessary to get your application up and running.


### How do I get set up? ###


1- download neo4j version(neo4j-community-3.5.30 or neo4j-community-3.5.31) from https://neo4j.com/download-center/#community

2- Go to path "neo4j-community-3.5.30\import" and put the cities.csv file inside that path

3- Open CMD as adminstrator and write command cd /d neo4j-community-3.5.30\bin

3- Run the server with command "neo4j start" and you will find a message with "service is started" 

4-Go to the Java task-backend project & run the commands

-mvn clean install

-5 Go to the target file inside the project & you will find the jar file is created then run the command 

java -jar "filename".jar.

